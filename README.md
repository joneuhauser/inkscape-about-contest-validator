# Inkscape About Contest Validator

Work-in-progress extension to validate about screen contest entries for Inkscape 1.3

Fully automatable:
- [x] Image size 750x625 px
- [x] Document was cleaned up
- [x] All visible text is converted to path
- [x] No raster graphic in drawing, except for patterns or masks

Partially automatable:
- [x] Background is covered, no round corners or small gaps
- [x] Author information in metadata
- [x] License information in metadata (CC-BY-SA-4.0)
- [x] Drawing was created with Inkscape
- [x] Load time

Not well automatable, but maybe some manual help is possible:
- [x] All visible text has an invisible corresponding text
- [x] All fonts have free license
- [ ] No offensive / political / otherwise inappropriate content
- [ ] Third-party elements must be credited properly and use a free license
