import context
from abc import ABC

import pytest
from contest_checker.contest_checker import ContestChecker

from inkex.tester import TestCase


class TestFile(TestCase):
    """Test a known good previous about screen entry."""

    def setUp(self):
        self.ext = ContestChecker()
        self.ext.parse_arguments(["tests/data/About_Screen2_v1-2_ChrisHildenbrand.svg"])
        self.ext.load_raw()

    def test_size(self):
        """Test the size of the document"""
        res, msg = self.ext.check_dimensions()
        self.assertEqual(res, 0)
        self.assertEqual(msg, "")

    def test_license(self):
        """Test the license of the document"""
        res, msg = self.ext.check_license()
        self.assertEqual(res, 0)
        self.assertEqual(
            msg,
            "CC BY-SA 4.0 license found: http://creativecommons.org/licenses/by-sa/4.0/",
        )

    def test_author(self):
        """Test author detection"""
        res, msg = self.ext.check_author()
        self.assertEqual(res, 0)
        self.assertEqual(msg, "Author found: Chris Hildenbrand")

    def test_no_visible_text_elements(self):
        """Test detection of visible text elements"""
        res, msg = self.ext.check_no_visible_text_elements()
        self.assertEqual(res, 0)
        self.assertEqual(msg, "")

    def test_corresponding_text_elements(self):
        """Test detection of corresponding text elements"""
        res, msg = self.ext.check_corresponding_texts()
        self.assertEqual(len(msg.splitlines()), 4)
        self.assertIn('text8074, content "Draw Freely."', msg)
        self.assertIn('text8078, content "INKSCAPE 1.2"', msg)
        self.assertIn('text8082, content "inkscape.org"', msg)
        self.assertEqual(res, 3)

    def test_font_license(self):
        """Test that fonts are listed for license checks"""
        res, msg = self.ext.check_text_font_licenses()
        self.assertEqual(len(msg.splitlines()), 4)
        self.assertIn("Linux Libertine O", msg)
        self.assertIn("Euphoria Script", msg)
        self.assertEqual(res, 3)

    def test_made_with_inkscape(self):
        """Tests that there is the <Created with Inkscape> comment"""
        res, msg = self.ext.check_made_with_inkscape()
        self.assertEqual(res, 0)
        self.assertEqual(msg, "")

    @pytest.mark.skip()
    def test_load_time(self):
        """Test the load time of the file"""
        res, msg = self.ext.check_load_time()
        self.assertEqual(res, 3)

        self.assertTrue(msg.splitlines()[0].startswith("Empty doc took avg"))
        self.assertTrue(msg.splitlines()[1].startswith("File took avg"))
        self.assertTrue(msg.splitlines()[2].startswith("Difference avg"))

    def test_no_raster_images(self):
        """Test detection of visible text elements"""
        res, msg = self.ext.check_no_raster_images()
        self.assertEqual(res, 0)
        self.assertEqual(msg, "")

    def test_document_cleaned(self):
        res, msg = self.ext.check_document_cleaned()
        self.assertEqual(
            msg.strip(), "Found unreferenced element clipoutline1 in defs2"
        )
        self.assertEqual(res, 1)

    def test_image_filled(self):
        res, msg = self.ext.check_image_completely_filled()
        self.assertEqual(res, 0)
        self.assertEqual(msg, "")


class TestBadFile(TestCase):
    """Test a manually generated file with a lot of problems."""

    def setUp(self):
        self.ext = ContestChecker()
        self.ext.parse_arguments(["tests/data/bad.svg"])
        self.ext.load_raw()

    def test_size(self):
        """Test the size"""
        res, msg = self.ext.check_dimensions()
        self.assertEqual(res, 1)
        self.assertEqual(msg, "Drawing size wrong. Found 793.70 x 1122.52")

    def test_license(self):
        """Test the license of the document"""
        res, msg = self.ext.check_license()
        self.assertEqual(res, 1)
        self.assertEqual(msg, "No cc:License tag found. File is proprietary")

    def test_author(self):
        """Test author detection"""
        res, msg = self.ext.check_author()
        self.assertEqual(res, 1)
        self.assertEqual(msg, "No author tag found")

    def test_no_visible_text_elements(self):
        """Test detection of visible text elements"""
        res, msg = self.ext.check_no_visible_text_elements()
        self.assertEqual(res, 1)
        self.assertEqual(len(msg.splitlines()), 3)
        self.assertIn("text236", msg)
        self.assertIn("text637-0-5", msg)
        self.assertIn(
            "Text element text236-1 is visible, but lies outside of the visible page",
            msg,
        )

    def test_corresponding_text_elements(self):
        """Test detection of corresponding text elements"""
        res, msg = self.ext.check_corresponding_texts()
        self.assertEqual(len(msg.splitlines()), 4)
        self.assertIn('text236-5, content "Some invisible text in Segoe UI"', msg)
        self.assertIn('text637, content "invisible text using opacity"', msg)
        self.assertIn('text637-0, content "invisible text using stroke-opacity"', msg)
        self.assertEqual(res, 3)

    def test_font_license(self):
        """Test that fonts are listed for license checks"""
        res, msg = self.ext.check_text_font_licenses()
        self.assertEqual(len(msg.splitlines()), 4)
        self.assertIn("Segoe UI", msg)
        self.assertIn("sans-serif", msg)
        self.assertEqual(res, 3)

    def test_no_raster_images(self):
        res, msg = self.ext.check_no_raster_images()
        self.assertEqual(len(msg.splitlines()), 2)
        self.assertIn("Raster image image859 is located inside a clip/mask", msg)
        self.assertIn("Raster image image827 found, not inside clip/mask", msg)

    def test_document_cleaned(self):
        res, msg = self.ext.check_document_cleaned()
        self.assertEqual(
            msg.strip(), "Found unreferenced element linearGradient3658 in defs2"
        )
        self.assertEqual(res, 1)

    def test_image_filled(self):
        res, msg = self.ext.check_image_completely_filled()
        self.assertEqual(res, 1)
        self.assertIn(
            "Share of pixels that changed from black to white (probably empty): 99.",
            msg,
        )
        self.assertIn(
            "Share of pixels that changed (probably semi-transparent without solid background): 99.5",
            msg,
        )


class TestRasterImage(TestCase):
    """Test a manually generated file with a lot of problems."""

    def setUp(self):
        self.ext = ContestChecker()
        self.ext.parse_arguments(["tests/data/raster image.svg"])
        self.ext.load_raw()

    def test_font_license(self):
        """Drawing contains no fonts"""
        res, msg = self.ext.check_text_font_licenses()
        self.assertIn("No font specifications found in invisible text.", msg)
        self.assertEqual(res, 0)

    def test_no_raster_images(self):
        """Drawing contains a linked raster image"""
        res, msg = self.ext.check_no_raster_images()
        self.assertIn("Raster image image9582 is located inside a clip/mask", msg)
        self.assertEqual(res, 2)

    def test_document_cleaned(self):
        """Drawing contains an unreferenced filter"""
        res, msg = self.ext.check_document_cleaned()
        self.assertEqual(
            msg.strip(), "Found unreferenced element filter5061-1-7-9-6-5 in defs687"
        )
        self.assertEqual(res, 1)

    def test_corresponding_text_elements(self):
        """Test corresponding text elements"""
        res, msg = self.ext.check_corresponding_texts()
        self.assertIn(
            "No invisible text elements found. Does the drawing not contain any text?",
            msg,
        )
        self.assertEqual(res, 3)

    def test_image_filled(self):
        """Image seems to be filled, might be an antialiasing issue. Partially fail is good enough"""
        res, msg = self.ext.check_image_completely_filled()
        self.assertIn(res, [0, 2])

class TestDefsCleanup(TestCase):
    def test_crhild_1_3(self):
        self.ext = ContestChecker()
        self.ext.parse_arguments(["tests/data/Inkscape_AboutScreen_1_3_Growth_Chris_Hildenbrand_V60ZYvA.svg"])
        self.ext.load_raw()

        res, msg = self.ext.check_document_cleaned()
        self.assertEqual(res, 0)