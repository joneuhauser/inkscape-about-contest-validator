"""Handles imports for unit tests"""

import os
import sys

# This is suggested by https://docs.python-guide.org/writing/structure/.
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

from contest_checker.contest_checker import ContestChecker

ContestChecker.__module__ = "ContestChecker"
