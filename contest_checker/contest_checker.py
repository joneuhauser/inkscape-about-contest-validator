#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) 2022 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""About screen contest checker"""

import sys
from pathlib import Path
import math
import inkex
from typing import Tuple, List, Optional
from inkex import command
from tempfile import TemporaryDirectory
import time
from PIL import Image
import numpy as np

# This bit of import fiddling is taken from
# https://stackoverflow.com/a/28154841/3298143, and it is necessary
# so the script can run both with -m (needed for pypi) as well as
# directly (needed for being an extensions submodule)
if __name__ == "__main__" and (__package__ is None or __package__ == ""):
    file = Path(__file__).resolve()
    parent, top = file.parent, file.parents[1]

    sys.path.append(str(top))
    try:
        sys.path.remove(str(parent))
    except ValueError:  # Already removed
        pass

    __package__ = "inkai"

PASS = 0
FAIL = 1
PARTIALLY_FAIL = 2
MANUAL = 3


RESULTS_DICT = {
    PASS: "Passed",
    PARTIALLY_FAIL: "Partially failed",
    FAIL: "Failed",
    MANUAL: "Check manually",
}


class ContestChecker(inkex.EffectExtension):
    """Show document information"""

    def __init__(self):
        super().__init__()
        self.visible = []
        self.invisible = []

    def check_dimensions(self) -> Tuple[int, str]:
        """Check that the document dimensions are exactly 750 x 625 (px)"""
        width = self.svg.viewport_width
        height = self.svg.viewport_height

        is_ok = math.isclose(width, 750) and math.isclose(height, 625)
        if is_ok:
            return PASS, ""
        else:
            return FAIL, f"Drawing size wrong. Found {width:.2f} x {height:.2f}"

    def check_license(self) -> Tuple[int, str]:
        """Check that the license is CC-BY-SA-4.0"""
        license = self.svg.xpath(r"/svg:svg/svg:metadata/rdf:RDF/cc:License")
        if len(license) == 0:
            return FAIL, "No cc:License tag found. File is proprietary"
        url = license[0].get(r"rdf:about", None)
        if url is None:
            return FAIL, "License tag does not contain rdf:about attribute"
        if url == "http://creativecommons.org/licenses/by-sa/4.0/":
            return PASS, f"CC BY-SA 4.0 license found: {url}"
        if url in [
            "http://creativecommons.org/publicdomain/zero/1.0/",
            "http://creativecommons.org/licenses/by/4.0/",
            "http://creativecommons.org/licenses/by-nc/4.0/",
            "http://creativecommons.org/licenses/by-nd/4.0/",
            "http://creativecommons.org/licenses/by-nc-nd/4.0/",
            "http://creativecommons.org/licenses/by-nc-sa/4.0/",
        ]:
            return FAIL, f"Known bad license: {url}"
        else:
            return MANUAL, f"Unknown license found: {url}"

    def check_author(self) -> Tuple[int, str]:
        """Check that author information exists in metadata"""

        creator = self.svg.xpath(
            r"/svg:svg/svg:metadata/rdf:RDF/cc:Work/dc:creator/cc:Agent/dc:title"
        )
        if len(creator) == 0:
            return FAIL, "No author tag found"
        creator_name = creator[0].text
        if len(creator_name.strip()) < 2:
            return FAIL, "Author tag is empty"
        return PASS, f"Author found: {creator_name}"

    def get_visible_invisible_texts(
        self,
    ) -> Tuple[List[inkex.TextElement], List[inkex.TextElement]]:
        """Return the visible and invisible text elements.
        These are cached because this is somewhat expensive and is needed
        by multiple checks"""
        if self.invisible or self.visible:
            return self.visible, self.invisible
        texts: List[inkex.BaseElement] = self.svg.xpath("//svg:text|//svg:flowRoot")
        self.visible: List[inkex.TextElement] = []
        self.invisible: List[inkex.TextElement] = []

        for text in texts:
            style = text.specified_style()
            if (
                style("display") == "none"
                or style("opacity") == 0
                or (
                    (style("fill") is None or style("fill-opacity") == 0)
                    and (style("stroke") is None or style("stroke-opacity") == 0)
                )
            ):
                self.invisible.append(text)
            else:
                # The "display:none" attribute could also be set on
                # any of the element's parents. The attribute is not inherited.
                parent = text.getparent()
                if parent is not None and parent.specified_style()("display") == "none":
                    self.invisible.append(text)
                    continue
                self.visible.append(text)
        return self.visible, self.invisible

    def check_no_visible_text_elements(self) -> Tuple[int, str]:
        """Check that there are no visible text elements inside the drawing region"""
        visible_texts, _ = self.get_visible_invisible_texts()

        state = PASS
        result = ""

        page_box: inkex.BoundingBox = inkex.BoundingBox(
            (0, self.svg.viewbox_width), (0, self.svg.viewbox_height)
        )
        for text in visible_texts:
            bbox = text.get_inkscape_bbox()
            if bbox.size[0] * bbox.size[1] == 0:
                # Text with zero bounding box according to Inkscape.
                if state == PASS:
                    state = PARTIALLY_FAIL
                result += f"Text element {text.get_id()} is visible, but has zero bounding box\n"
            elif (
                (page_box.x.maximum < bbox.x.minimum)
                or (page_box.x.minimum > bbox.x.maximum)
                or (page_box.y.maximum < bbox.y.minimum)
                or (page_box.y.minimum > bbox.y.maximum)
            ):
                # Text elements that lie completely outside may be visible
                result += f"Text element {text.get_id()} is visible, but lies outside of the visible page\n"
            else:
                state = FAIL
                result += (
                    f"Text element {text.get_id()} at {bbox} is probably visible\n"
                )

        return state, result

    def check_corresponding_texts(self) -> Tuple[int, str]:
        """Print the invisible text elements"""
        _, invisible_texts = self.get_visible_invisible_texts()

        def append_with_space(text: Optional[str], result: str):
            if text is not None and len(text.strip()) > 0:
                if len(result) != 0:
                    result += " "
                result += text
            return result

        def append_recursively(element, result):
            result = append_with_space(element.text, result)
            for child in element:
                result = append_recursively(child, result)
            result = append_with_space(element.tail, result)
            return result

        result = ""
        for text in invisible_texts:
            result += f'Found invisible text {text.get_id()}, content "{append_recursively(text, "")}" at {text.get_inkscape_bbox()}\n'

        if result != "":
            return (
                MANUAL,
                result
                + "Check if these texts have corresponding visual (path) text elements",
            )
        return (
            MANUAL,
            "No invisible text elements found. Does the drawing not contain any text?",
        )

    def check_text_font_licenses(self) -> Tuple[int, str]:
        """Print used fonts so their license can be checked."""
        _, invisible_texts = self.get_visible_invisible_texts()

        result = []

        def recursive_font(element, result):
            result += [element.specified_style()("font-family")]
            for child in element:
                return recursive_font(child, result)

        for text in invisible_texts:
            recursive_font(text, result)
        fonts = set(result)
        if len(fonts) == 0:
            return (
                PASS,
                "No font specifications found in invisible text. Does the drawing not contain any text?",
            )
        else:
            return (
                MANUAL,
                f"The following fonts were found in invisible text: \n"
                + "\n".join(fonts)
                + "\nCheck their license.",
            )

    def check_made_with_inkscape(self) -> Tuple[int, str]:
        """Checks that there is an inkscape:version attr on the SVG element"""
        if self.svg.root.get("inkscape:version", None) is not None:
            return PASS, ""
        return MANUAL, "No inkscape:version attribute found. Please check manually."

    def check_load_time(self) -> Tuple[int, str]:
        """Display the load time"""

        with TemporaryDirectory(prefix="inkscape-command") as tmpdir:
            svg_file = command.write_svg(self.svg.root, tmpdir, "input.svg")
            object_to_query = (self.svg.xpath("//svg:path|//svg:rect") or self.svg)[
                0
            ].get_id()
            empty = tmpdir + "/empty.svg"
            with open(empty, "w") as f:
                f.write(
                    '<svg><rect id="rect1" x="0" y="0" width="10" height="10"/><svg>'
                )

            results = []
            results_empty = []
            for i in range(3):
                start_time = time.time()
                command.inkscape(
                    empty, "-X", "-Y", "-W", "-H", query_id=object_to_query
                )
                results_empty += [time.time() - start_time]
                start_time = time.time()
                command.inkscape(
                    svg_file, "-X", "-Y", "-W", "-H", query_id=self.svg[0].get_id()
                )
                results += [time.time() - start_time]
            stats = []
            for data in [results_empty, results]:
                stats += [[sum(data) / len(data), min(data), max(data)]]
            stats += [[stats[1][i] - stats[0][i] for i in range(3)]]
            stats_stringed = [
                f"avg: {avg:.2f}, min: {minn:.2f}, max: {maxx:.2f}"
                for avg, minn, maxx in stats
            ]

        return (
            MANUAL,
            f"Empty doc took {stats_stringed[0]}\nFile took {stats_stringed[1]}\nDifference {stats_stringed[2]}",
        )

    def element_location(self, element):
        """Find out if an element is inside a clip or mask element within defs"""
        parent = element
        inClipOrMask = False
        defs = False
        while True:
            parent = parent.getparent()
            if parent == self.svg or parent is None:
                break
            if isinstance(parent, (inkex.ClipPath, inkex.Mask)):
                inClipOrMask = True
            if isinstance(parent, (inkex.Defs)):
                defs = True
        return inClipOrMask, defs

    def check_no_raster_images(self) -> Tuple[int, str]:
        """Test that there are no raster images in the file"""

        images = self.svg.xpath("//svg:image")
        result = PASS
        message = ""
        for image in images:
            href = image.get("xlink:href", None)
            if href is not None and (
                href.startswith("data:image/png")
                or href.startswith("data:image/jpeg")
                or href.strip().endswith(".jpg")
                or href.strip().endswith(".png")
            ):
                # We have found a raster image. Check its location.
                clipmask, defs = self.element_location(image)
                if clipmask and defs:
                    if result == PASS:
                        result = PARTIALLY_FAIL
                    message += (
                        f"Raster image {image.get_id()} is located inside a clip/mask\n"
                    )
                else:
                    result = FAIL
                    message += (
                        f"Raster image {image.get_id()} found, not inside clip/mask\n"
                    )
        return result, message

    def check_image_completely_filled(self) -> Tuple[int, str]:
        """Find empty spots by comparing two exported PNGs"""
        # First backup those properties.
        pagecolor = self.svg.namedview.get("pagecolor")
        pageopacity = self.svg.namedview.get("inkscape:pageopacity")

        diffed_frames = []
        result = PASS
        message = ""
        with TemporaryDirectory(prefix="inkscape-command") as tmpdir:
            for color in ["#ffffff", "#000000"]:
                self.svg.namedview.set("pagecolor", color)
                self.svg.namedview.set("inkscape:pageopacity", "1.0")
                svg_file = command.write_svg(
                    self.svg.root, tmpdir, f"input_{color}.svg"
                )
                png = tmpdir + f"/output_{color}.png"
                command.inkscape(svg_file, export_filename=png)

                frame = np.array(Image.open(png).convert("RGB").getdata())
                frame -= inkex.Color(color).red
                diffed_frames += [frame]

        share_changed_color = np.count_nonzero(
            np.min(diffed_frames[1] - diffed_frames[0] - 255, axis=1)
        ) / len(diffed_frames[0])
        share_empty = 1 - np.count_nonzero(
            np.min(np.abs(diffed_frames[1] - diffed_frames[0]), axis=1)
        ) / len(diffed_frames[0])

        if share_empty > 0:
            result = FAIL
        elif share_changed_color > 0:
            result = PARTIALLY_FAIL

        if result != PASS:
            message = (
                "The image was exported two times, using black and white screen color.\n"
                f"Share of pixels that changed from black to white (probably empty): {share_empty * 100:.2f}\n"
                f"Share of pixels that changed (probably semi-transparent without solid background): {share_changed_color * 100:.2f}\n"
            )

        self.svg.namedview.set("pagecolor", pagecolor)
        self.svg.namedview.set("inkscape:pageopacity", pageopacity)

        return result, message

    def check_document_cleaned(self) -> Tuple[int, str]:
        hrefs = self.svg.xpath("//svg:defs")

        result = PASS
        message = ""

        for href in hrefs:
            for child in href:
                id = child.get_id()
                href_elements = self.svg.getElementsByHref(id)
                clip_elements = self.svg.getElementsByHref(id, attribute="clip-path")
                mask_elements = self.svg.getElementsByHref(id, attribute="mask")
                mask_elements = self.svg.getElementsByHref(id, attribute="mask")
                path_effect_elements = self.svg.xpath(
                    f'//*[contains(@inkscape:path-effect, "#{id}")]'
                )
                style_references = self.svg.getElementsByStyleUrl(id)
                if (
                    not href_elements
                    and not clip_elements
                    and not mask_elements
                    and not path_effect_elements
                    and not style_references
                ):
                    # Element not referenced
                    result = FAIL
                    message += f"Found unreferenced element {id} in {href.get_id()}\n"

        return result, message

    def effect(self):
        results = {}

        for check in [
            self.check_dimensions,
            self.check_author,
            self.check_license,
            self.check_load_time,
            self.check_text_font_licenses,
            self.check_made_with_inkscape,
            self.check_no_raster_images,
            self.check_document_cleaned,
            self.check_no_visible_text_elements,
            self.check_corresponding_texts,
            self.check_image_completely_filled,
        ]:
            res, message = check()
            results[res] = results.get(res, 0) + 1

            inkex.errormsg(f"[{RESULTS_DICT[res]}] {check.__name__}")
            if message != "":
                inkex.errormsg(message)
                inkex.errormsg("")

        inkex.errormsg(
            "Results: "
            + ", ".join(f"{results.get(i, 0)} {RESULTS_DICT[i]}" for i in RESULTS_DICT)
        )


if __name__ == "__main__":
    ContestChecker().run()
